from django.shortcuts import render
from django.views import View
from django.views.generic import TemplateView
from rest_framework import viewsets

from myapp.models import Cat
from myapp.serializers import CatSerializer


# Create your views here.
class CatListView(View):
    def get(self, request):
        context = {'cats': Cat.objects.all()}
        return render(request, "index.html", context)

class CatsViewSet(viewsets.ModelViewSet):
    queryset = Cat.objects.all()
    serializer_class = CatSerializer