from django.db import models

# Create your models here.
class Cat(models.Model):
    name = models.CharField(verbose_name="Имя", max_length=32)
    age = models.IntegerField(verbose_name="Возраст")
