window.onload = fetchCats;

async function fetchCats() {
    fetch(`http://0.0.0.0:8000/api/v2/cats/`)
        .then(response => response.json())
        .then(json => {
            for (let i = 0; i < json.length; i++) {
                var ul = document.getElementById('cats')
                var li = document.createElement('li')
                li.appendChild(document.createTextNode(`${json[i].name}, ${json[i].age}`))
                ul.appendChild(li)
            }
        })
}